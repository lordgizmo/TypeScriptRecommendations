import { ConfigurationTests } from "./Configurations/index.test.js";

/**
 * Registers the tests.
 */
function Tests(): void
{
    suite(
        "TypeScriptRecommendations",
        () =>
        {
            ConfigurationTests();
        });
}

Tests();
